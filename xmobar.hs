module My.Xmobar
  ( spawnManyXMobar
  )
where

import Conduit
import Control.Monad
import Data.ByteString
import System.IO
import System.Posix.IO

import XMonad.Core (xfork)

spawnManyXMobar :: Int -> IO Handle
spawnManyXMobar c = do
    (rd, wr) <- createPipe
    wh <- fdToHandle wr
    hSetBuffering wh LineBuffering

    stdins <- xmobars
    _ <- fanOut rd stdins
    pure wr

  where
    xmobars :: IO [Handle]
    xmobars = replicateM c (spawnPipe "xmobar")

    fanOut :: Handle -> [Handle] -> IO ()
    fanOut = do
      _ <-  runConduit (sourceHandle rd .| sinkManyHandles handles)
      pure ()
