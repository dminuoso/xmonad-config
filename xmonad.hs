{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# OPTIONS_GHC -Wall -Werror      #-}

import           XMonad
import           XMonad.Hooks.DynamicLog

import           XMonad.Prompt (XPConfig(..))
import           XMonad.Prompt.FuzzyMatch (fuzzyMatch, fuzzySort)
import           XMonad.Prompt.XMonad (xmonadPromptC)
import           XMonad.Prompt.RunOrRaise (runOrRaisePrompt)
import qualified XMonad.Hooks.EwmhDesktops as ED
import qualified XMonad.Layout.IndependentScreens as IS

import qualified Data.Map as M
import           Data.Monoid

import qualified Solarized as S

import           StatusBar
import           Utils (RegisteredAction(..), KeyCombo)
import           Urgency (withUrgency, urgencyActions)
import           Windows (windowActions)
import           KeyboardLayout (keyboardActions)
import           Screenshot (screenshotActions)
import           Debug (debugActions)

main :: IO ()
main = do
  nScreens <- IS.countScreens
  conf <- manyXMobar mobarPP (myConfig nScreens)
  xmonad (withUrgency (ED.ewmh conf))

myLayout :: Choose Tall Full a
myLayout = Tall 1 (3/100) (1/2) ||| Full

eventHooks :: Event -> X All
eventHooks = onScreenChange restartXmonad

onScreenChange :: X () -> Event -> X All
onScreenChange action (RRScreenChangeNotifyEvent {}) = action >> pure (All True)
onScreenChange _ _ = pure (All True)

myConfig :: ScreenId -> XConfig (Choose Tall Full)
myConfig nScreens =
  def { terminal = "xfce4-terminal"
      , modMask = mod4Mask
      , keys = myKeys
      , handleEventHook = eventHooks
      , workspaces = IS.withScreens nScreens (workspaces def)
      , layoutHook = myLayout
      }

mobarPP :: PP
mobarPP = xmobarPP
  { ppCurrent = xmobarColor S.green "" . wrap "<" ">"
  , ppTitle   = xmobarColor S.red "" . shorten 40
  }

myActions :: XConfig Layout -> [RegisteredAction]
myActions c = promptAction c : allActions c

promptAction :: XConfig Layout -> RegisteredAction
promptAction conf
    | let mm = modMask conf
    = RA { raCombo = (mm, xK_x)
         , raCmd = ""
         , raDescr = "Start the xmonad prompt"
         , raAction = prompt conf }

prompt :: XConfig Layout -> X ()
prompt conf = xmonadPromptC promptCommands fuzzyXpc
  where
    promptCommands :: [(String, X ())]
    promptCommands = toCommand <$> matching

    matching :: [RegisteredAction]
    matching = filter (not . null . raCmd) (allActions conf)

    toCommand :: RegisteredAction -> (String, X ())
    toCommand RA{raCmd, raAction} = (raCmd, raAction)

allActions :: XConfig Layout -> [RegisteredAction]
allActions = windowActions
          <> keyboardActions
          <> baseActions
          <> urgencyActions
          <> screenshotActions
          <> debugActions

myKeys :: XConfig Layout -> M.Map KeyCombo (X ())
myKeys conf = go (myActions conf)
  where
    go :: [RegisteredAction] -> M.Map KeyCombo (X ())
    go xs = M.fromList (lower <$> xs)

    lower :: RegisteredAction -> (KeyCombo, X ())
    lower RA{raCombo, raAction} = (raCombo, raAction)

baseActions :: XConfig l -> [RegisteredAction]
baseActions conf
  | let mm = modMask conf
  = [ RA { raCombo = (mm .|. shiftMask, xK_Return)
         , raCmd = ""
         , raDescr = "Start a new terminal"
         , raAction = spawn (XMonad.terminal conf) }

    , RA { raCombo = (mm, xK_p)
         , raCmd = ""
         , raDescr = "Run or raise an application"
         , raAction = ror }

    , RA { raCombo = (mm, xK_q)
         , raCmd = "xmonad-restart"
         , raDescr = "Recompile and restart xmonad"
         , raAction = restartXmonad }
    ]

ror :: X ()
ror = runOrRaisePrompt fuzzyXpc { maxComplRows = Just 1
                                , alwaysHighlight = True }

fuzzyXpc :: XPConfig
fuzzyXpc = S.solDark { font = "xft:PragmataPro Mono:style=Regular"
                     , searchPredicate = fuzzyMatch
                     , sorter = fuzzySort }

restartXmonad :: X ()
restartXmonad = spawn "if type xmonad; then xmonad --recompile && xmonad --restart; else xmessage xmonad not in \\$PATH: \"$PATH\"; fi"
