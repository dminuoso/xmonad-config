module Utils
  ( KeyCombo
  , RegisteredAction(..)
  )
where

import XMonad (KeyMask, KeySym)
import XMonad.Core (X)

type KeyCombo = (KeyMask, KeySym)
data RegisteredAction = RA { raCombo :: KeyCombo
                           , raCmd :: String
                           , raDescr :: String
                           , raAction :: X ()
                           }

 
