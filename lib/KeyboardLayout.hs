module KeyboardLayout
  ( keyboardActions
  )
where

import           Data.Foldable (asum)
import           Data.Char (isSpace)
import           Data.List (stripPrefix)
import qualified Data.Map as M

import           XMonad
import           XMonad.Core (X, XConfig(..))
import           XMonad.Util.Run (runProcessWithInput, safeSpawn)

import           Utils (RegisteredAction(..), KeyCombo)

currentKbLayout :: X String
currentKbLayout = do
    out <- runProcessWithInput "setxkbmap" ["-query"] ""

    let r = asum (findLayout <$> lines out)
    case r of
        Nothing  -> pure ""
        Just lay -> pure lay
  where
    findLayout :: String -> Maybe String
    findLayout = fmap skipWS . stripPrefix "layout:"

    skipWS :: String -> String
    skipWS = dropWhile isSpace

nextKbLayout :: String -> String
nextKbLayout "us" = "de"
nextKbLayout _    = "us"

setKbLayout :: String -> X ()
setKbLayout s = safeSpawn "setxkbmap" [s]

rotateLayout :: X ()
rotateLayout = (setKbLayout . nextKbLayout) =<< currentKbLayout

keyboardActions :: XConfig l -> [RegisteredAction]
keyboardActions conf
  | let mask = modMask conf
  = [ RA { raCombo = (mask .|. controlMask, xK_space)
         , raCmd = "keyboard-layout-rotate"
         , raDescr = "Rotate between available keyboard layouts"
         , raAction = rotateLayout
         }
    ]
