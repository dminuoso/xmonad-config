module Screenshot
  ( screenshotActions
  )
where


import           XMonad
import           XMonad.Core (X, XConfig(..), spawn)
import           XMonad.Util.Ungrab (unGrab)

import           Utils (RegisteredAction(..))

screenshotActions :: XConfig l -> [RegisteredAction]
screenshotActions conf
  | let mm = modMask conf
  = [ RA { raCombo = (mm, xK_s)
         , raCmd = "screenshot-select"
         , raDescr = "Take a screenshot of selected window or region"
         , raAction = ssSelect
         }
    ]

ssSelect :: X ()
ssSelect = unGrab >> spawn "scrot screen_%Y-%m-%d-%H-%M-%S.png --select -e 'mv $f ~/images/screenshots/' "
