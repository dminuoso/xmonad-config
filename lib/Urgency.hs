{-# LANGUAGE FlexibleContexts #-}
module Urgency
  ( urgencyActions
  , withUrgency
  )
where

import           XMonad
import           XMonad.Core (LayoutClass, XConfig(..))
import qualified XMonad.Hooks.UrgencyHook as UH
import           XMonad.Util.NamedWindows (getName)
import qualified XMonad.StackSet as W

import           Utils
import           Notify (notify)

data LibNotifyUrgencyHook = LibNotifyUrgencyHook deriving (Read, Show)

instance UH.UrgencyHook LibNotifyUrgencyHook where
    urgencyHook LibNotifyUrgencyHook w = do
        name     <- getName w
        Just idx <- fmap (W.findTag w) $ gets windowset
        notify [show name, "workspace " ++ idx]

withUrgency :: (LayoutClass l Window) => XConfig l -> XConfig l
withUrgency = UH.withUrgencyHook LibNotifyUrgencyHook

urgencyActions :: XConfig l -> [RegisteredAction]
urgencyActions conf
  | let mm = modMask conf
  = [ RA { raCombo = (mm, xK_backslash)
         , raCmd = "urgent-follow"
         , raDescr = "Focus window that has sent urgent hint and clear it"
         , raAction = consumeUrgent }
    ]

consumeUrgent :: X ()
consumeUrgent = UH.focusUrgent >> UH.clearUrgents
