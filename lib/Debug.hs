module Debug
  ( debugActions
  )
where

import Data.Foldable

import           XMonad
import           XMonad.Core (X, XConfig(..))
import           XMonad.Hooks.DebugStack (debugStackString, debugStackFullString)

import           Utils (RegisteredAction(..))
import           Notify (xmessage)

import qualified Solarized as S

debugActions :: XConfig l -> [RegisteredAction]
debugActions conf 
  | let mask = modMask conf
  = [ RA { raCombo = (mask, xK_d)
         , raCmd = "debug-stack"
         , raDescr = "Debug the current workspace in the stack set"
         , raAction = debugPrompt
         }

    , RA { raCombo = (mask .|. shiftMask, xK_d)
         , raCmd = "debug-stack-full"
         , raDescr = "Debug the all workspaces in the stack set"
         , raAction = debugFullPrompt
         }
     ]

debugPrompt :: X ()
debugPrompt = debugStackString >>= xmessage

debugFullPrompt :: X ()
debugFullPrompt = debugStackFullString >>= xmessage

getEWMHTitle       :: String -> Window -> X String
getEWMHTitle sub w =  do
  a <- getAtom $ "_NET_WM_" ++ (if null sub then "" else '_':sub) ++ "_NAME"
  (Just t) <- withDisplay $ \d -> io $ getWindowProperty32 d a w
  return $ map (toEnum . fromEnum) t
