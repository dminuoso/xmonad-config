{-# LANGUAGE FlexibleContexts #-}
module StatusBar
  ( manyXMobar
  )
where

import           Control.Monad
import           Data.Foldable
import qualified Data.Map as M
import           XMonad

import           XMonad.Hooks.DynamicLog
import           XMonad.Util.NamedWindows
import           XMonad.Util.Run
import           XMonad.Util.WorkspaceCompare

import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.UrgencyHook
import qualified XMonad.Layout.IndependentScreens as IS
import           XMonad.Layout.LayoutModifier

import qualified Solarized as S
import           XMonad.Util.SpawnOnce

import Utils

mobarPP :: PP
mobarPP = xmobarPP
  { ppCurrent = xmobarColor S.green "" . wrap "<" ">"
  , ppTitle   = xmobarColor S.red "" . shorten 40
  }

manyXMobar :: LayoutClass l Window
           => PP        -- ^ the pretty printing options
           -> XConfig l -- ^ the base config
           -> IO (XConfig (ModifiedLayout AvoidStruts l))
manyXMobar pp conf = do
    n <- IS.countScreens
    hs <- traverse spawnXMonad [0..n - 1]
    return $ docks $ conf
        { layoutHook = avoidStruts (layoutHook conf)
        , logHook = do
                        logHook conf
                        for_ hs (\h -> dynamicLogWithPP pp { ppOutput = hPutStrLn h })
        }
 where
    spawnXMonad n = spawnPipe ("xmobar -x " <> show n)
