module Notify
  ( notify
  , xmessage
  )
where

import System.IO (hPutStr, hClose)
import Control.Monad.IO.Class (liftIO)

import XMonad.Core (X)
import XMonad.Util.Run (safeSpawn, spawnPipe)

notify :: [String] -> X ()
notify what = safeSpawn "notify-send" what

xmessage :: String -> X ()
xmessage s = liftIO $ do
  h <- spawnPipe "xmessage -file -"
  hPutStr h s
  hClose h
