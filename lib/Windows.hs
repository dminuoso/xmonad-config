{-# LANGUAGE NamedFieldPuns #-}
module Windows
  ( windowActions
  )
where

import           XMonad
import           XMonad.Actions.NoBorders (toggleBorder)
import           XMonad.Core (XConfig(..))
import           XMonad.Layout (IncMasterN(..), Resize(..))
import           XMonad.Operations (sendMessage, windows, killWindow)
import           XMonad.Hooks.ManageDocks (ToggleStruts(..))
import qualified XMonad.Layout.IndependentScreens as IS
import qualified XMonad.StackSet as W

import           Utils (RegisteredAction(..), KeyCombo)
import           Notify (notify)

withFocusedD :: (Window -> X ()) -> X ()
withFocusedD f = withWindowSet $ \w -> do
  case W.peek w of
    Just x  -> f x
    Nothing -> notify ["Nothing in focus"]

killD :: X ()
killD = withFocusedD killWindow

windowActions :: XConfig Layout -> [RegisteredAction]
windowActions c = windowBaseActions c
               <> independentScreenActions c

windowBaseActions :: XConfig Layout -> [RegisteredAction]
windowBaseActions conf
  | let mm = modMask conf
  = [ RA { raCombo = (mm, xK_Tab)
         , raCmd = "focus-move-down"
         , raDescr = "Move focus down to the next window in the stack set"
         , raAction = windows W.focusDown }

    , RA { raCombo = (mm .|. shiftMask, xK_Tab)
         , raCmd = "focus-move-up"
         , raDescr = "Move focus up to the next window"
         , raAction = windows W.focusUp }

    , RA { raCombo = (mm, xK_t)
         , raCmd = "focus-sink"
         , raDescr = "Sink focused window back into tiling"
         , raAction = withFocused (windows . W.sink) }

    , RA { raCombo = (mm .|. shiftMask, xK_c)
         , raCmd = "focus-kill"
         , raDescr = "Kill the focused window with prejudice"
         , raAction = killD }

    , RA { raCombo = (mm, xK_g)
         , raCmd = "focus-toggle-borders"
         , raDescr = "Toggle window borders"
         , raAction = withFocused toggleBorder }

    , RA { raCombo = (mm, xK_Return)
         , raCmd = "focus-swap-master"
         , raDescr = "Swap the focused window and the master window"
         , raAction = windows W.swapMaster }

    , RA { raCombo = (mm, xK_h)
         , raCmd = "master-shrink"
         , raDescr = "Shrink the master area"
         , raAction = sendMessage Shrink }

    , RA { raCombo = (mm, xK_l)
         , raCmd = "master-expand"
         , raDescr = "Expand the master area"
         , raAction = sendMessage Expand }

    , RA { raCombo = (mm, xK_comma)
         , raCmd = "master-num-increment"
         , raDescr = "Increment the number of windows in the master area"
         , raAction = sendMessage (IncMasterN 1) }

    , RA { raCombo = (mm, xK_period)
         , raCmd = "master-num-decrement"
         , raDescr = "Deincrement the number of windows in the master area"
         , raAction = sendMessage (IncMasterN (-1)) }

    , RA { raCombo = (mm, xK_space)
         , raCmd = "layout-rotate"
         , raDescr = "Rotate through the available layout algorithms"
         , raAction = sendMessage NextLayout }

    , RA { raCombo = (mm .|. shiftMask, xK_space)
         , raCmd = "layout-reset"
         , raDescr = "Reset the layouts on the current workspace to default"
         , raAction = setLayout (layoutHook conf) }

    , RA { raCombo = (mm, xK_b)
         , raCmd = "struts-toggle"
         , raDescr = "Toggle struts"
         , raAction = sendMessage ToggleStruts }

    , RA { raCombo = (mm .|. shiftMask, xK_p)
         , raCmd = "pull-everything"
         , raDescr = "Pull all windows to the current workspace"
         , raAction = pullAllWindows }
    ]

-- Pulls all windows to the current workspace.
pullAllWindows :: X ()
pullAllWindows = windows $ \ws ->
    let cur = W.currentTag ws
        win = W.allWindows ws

    in compose (W.shiftWin cur <$> win) ws

  where
    compose :: [a -> a] -> a -> a
    compose = foldr (.) id

screenActions :: XConfig l -> [RegisteredAction]
screenActions conf
  | let mm = modMask conf
  = 
    -- mod-[1..9] %! Switch to workspace N
    -- mod-shift-[1..9] %! Move client to workspace N
    [ RA { raCombo = (mm .|. m, k)
         , raCmd = ""
         , raDescr = ""
         , raAction = windows (f i) }
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]

    <>

    -- mod-{w,e,r} %! Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r} %! Move client to screen 1, 2, or 3
    [ RA { raCombo = (mm .|. m, key)
         , raCmd = ""
         , raDescr = ""
         , raAction = screenWorkspace sc >>= flip whenJust (windows . f) }

        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0,1,2]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

independentScreenActions :: XConfig l -> [RegisteredAction]
independentScreenActions conf
  | let mm = modMask conf
  = 
    -- Switch to virtual workspaces
    [ RA { raCombo = (mm .|. m, key)
         , raCmd = ""
         , raDescr = ""
         , raAction = windows (IS.onCurrentScreen action ws) }

        | (key, ws) <- zip [xK_1 .. xK_9] (IS.workspaces' conf)
        , (action, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]

    <>
    -- mod-{w,e,r} %! Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r} %! Move client to screen 1, 2, or 3
    [ RA { raCombo = (mm .|. mm, key)
         , raCmd = ""
         , raDescr = ""
         , raAction = screenWorkspace sc >>= flip whenJust (windows . f) }

        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0,1,2]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

    <>
    [ RA { raCombo = (mm .|. m, key)
         , raCmd = ""
         , raDescr = ""
         , raAction = windows (W.view ws) }

        | (key, virt) <- zip [xK_1 .. xK_9] ['0'..]
        , (m, s) <- [(controlMask, 1), (mod1Mask, 2)]
        , let ws = IS.marshall s [virt]]
